import { Component } from "react";

class InputMessage extends Component {

    inputChangeHandle = (event) => {
        console.log("Input thay đổi");
        //console.log(event.target.value);

        this.props.messageInputChangeHandlerProp(event.target.value);
    }

    buttonClickHandler = () => {
        console.log("Nút click được bấm");
        
        this.props.messageOutputChangeHandlerProp();
    }

    render() {
        return (
            <div>
                <div className="row mt-2">
                    <div className="col-12">
                        <label className="form-label">Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <input id="abc" className="form-control" placeholder="Nhập message" onChange={this.inputChangeHandle}  />
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <button className="btn btn-success" onClick={this.buttonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default InputMessage;