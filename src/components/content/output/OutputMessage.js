import { Component } from "react";

import likeImg from "../../../assets/images/like.png";

class OutputMessage extends Component {
    render() {
        return (
            <div>
                <div className="row mt-3">
                    <div className="col-12">
                        {this.props.messageOutputProp.map((element, index) => {
                            return  <p key={index}>{element}</p>
                        })}
                    </div>
                </div>
                { this.props.likeDisplayProp ? 
                    <div className="row mt-2">
                        <div className="col-12">
                            <img src={likeImg} width={100} alt="like"/>
                        </div>
                    </div> : null }
            </div>  
        )
    }
}

export default OutputMessage;